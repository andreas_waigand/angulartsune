PouchDB.debug.enable('*');

(function () {
    var app = angular.module("pouch.startup", ["pouch.decks", "pouch.cards", "pouch.base"]);

    app.factory('repDocument', function repDocumentFactory() {
        return function (cardid, deck, EF, rep, nextRep) {
            if (typeof(EF) === 'undefined') EF = 2.5;
            if (typeof(rep) === 'undefined') rep = 1;
            if (typeof(nextRep) === 'undefined') nextRep = new Date().toJSON();
            return {
                "cardid": cardid,
                "EF": EF,
                "rep": rep,
                "nextRep": nextRep,
                "type": "repitition",
                "deck": deck
            }
        }
    });

    app.constant('pouch.config', {
        host: "http://193.196.7.76:8080"
    });

    app.service('pouchDBService', ['$q', '$timeout', '$rootScope', 'repDocument', 'pouch.config', "pouchDBService.decks","pouchDBService.cards", "pouchDB","userNameConverter", function ($q, $timeout, $rootScope, repDocumentFactory, config, deckService, learnService, pouchDBFactory,userNameConverter) {


        function sortByNextRepetitionDate(a, b) {
            return new Date(a.rep.nextRep) - new Date(b.rep.nextRep);
        }

        function resetAllFields(self){
            self.mainDb = undefined;
            self.startUp =  undefined;
            self.username = "";
            self.remoteSync= undefined;
            self.commonDBSync = undefined;
            self._RepsByDeck = {};
            self._CardsForDeck= {};
            self._CardRepForDeck= {};

            deckService.init();
            learnService.init();
        }

        var pouchDBService = {

            mainDb: undefined,
            host: config.host,
            //If first startup is done
            startUp: undefined,
            username: "",
            remoteSync:undefined,
            commonDBSync:undefined,
            /**
             * This function stops all live syncs and replication which was activated.
             * It should be used when the user logs out.
             */

            stopReplication: function(){
                this.remoteSync && this.remoteSync.cancel();
                this.commonDBSync && this.commonDBSync.cancel();
            },

            resetAllField: function(){
              resetAllFields(this);
            },

            /**
             * Prepares all neccessary data and services on startup
             * It returns a promise which gets resolves when the startup is successful.
             * @param online True if user is online, false if offline
             * @param username Username of current user
             * @returns {d.promise|promise|fd.g.promise|.n.promise}
             */

            generalStartup: function (online, username) {
                if(this.startUp)
                 return this.startUp.promise;
                var self = this;
                resetAllFields(this);
                self.startUp = $q.defer();
                self.username = username;
                self.mainDb = pouchDBFactory(userNameConverter(self.username));
                if (online) {
                    self.onlineStartup().then(function (result) {
                        self.offlineStartup().then(function (res) {
                            self.startUp.resolve();
                        });
                    }).catch(function (error) {

                    });
                } else {
                    self.offlineStartup().then(function (res) {
                        self.startUp.resolve();
                    });
                }

                return self.startUp.promise;
            },


            offlineStartup: function () {
                return deckService.getDecksGeneralInformation();
            },

            startSyncOfUserDb: function () {
                var syncDef = $q.defer();
                var self = this;
                var remoteDb = pouchDBFactory(self.host + userNameConverter(self.username));
                var remoteCommonDb = pouchDBFactory(self.host + "common");
                var commonDb = pouchDBFactory("common");
                var repl = PouchDB.sync(remoteDb, self.mainDb);
                self.commonDBSync = PouchDB.sync(remoteCommonDb,commonDb,{live:true});
                repl.on('complete', function (res) {
                    self.remoteSync = PouchDB.sync(remoteDb,self.mainDb,{live: true});
                    syncDef.resolve(res);
                }).on('error', function (err) {
                    //Hack for PouchDB Issue #135
                    syncDef.resolve(err);
                });

                return syncDef.promise;
            },

            replicateSingleDeckChangesToOffline: function (deckname) {
                var singleReplofDeckDone = $q.defer();
                singleReplofDeckDone.promise.deck = deckname;

                var self = this;
                PouchDB.replicate(self.host + deckname, deckname).on('complete',
                    function (res) {

                        singleReplofDeckDone.resolve();
                    }).on('error', function (res) {

                        singleReplofDeckDone.resolve();
                    });

                return singleReplofDeckDone.promise;
            },

            replicateAllOnlineDeckChangesToOffline: function () {
                var self = this;
                var allReplicationOfDecksDone = $q.defer();
                var allPromises = [];
                var waitForPromisesFromSingleDeckUpdates = $q.defer();
                deckService.getDeckIDs().then(function (result) {
                    angular.forEach(result, function (deckname) {
                        var def = self.replicateSingleDeckChangesToOffline(deckname);
                        allPromises.push(def);
                    });
                    waitForPromisesFromSingleDeckUpdates.resolve();
                }).catch(function (error) {
                });
                waitForPromisesFromSingleDeckUpdates.promise.then(function () {
                    $q.all(allPromises).then(function (res) {
                        allReplicationOfDecksDone.resolve();
                    });
                });
                return allReplicationOfDecksDone.promise;
            },

            onlineStartup: function () {
                var startDef = $q.defer();
                var self = this;
                self.startSyncOfUserDb().then(function (result) {

                    self.replicateAllOnlineDeckChangesToOffline().then(function (result) {

                        startDef.resolve();
                    });

                }).catch(function (error) {
                    //For some reason replication failed, so I wont try replicating the decks.
                    //Just retry regularly and go for offline for now
                    startDef.resolve();
                });

                return startDef.promise;
            }

        };


        return pouchDBService;
    }]);

})();




