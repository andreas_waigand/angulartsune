(function(){
    var app = angular.module("pouch.update", ["pouch.base"]);

    /**
     * Provides all functionallity needed for updating skills in the Gräte
     */




    app.constant("pouch.update.config",{
        viewForDoneCheck: "tsune/countRepsInPeriod",
        viewForCountingCards: "cardDocs/countCards",
        considerDoneAfterDays: 1,
        progresslisttype: "progresslist",
        idOfProgressList: "tsune-progresslist",
        genericProgressList: {
            _id: undefined,
            type: undefined,
            module: "tsune",
            skills: {}
        }}
    );


    app.service("pouchDBService.update", ['$rootScope', "$q", "pouch.update.config", 'pouchDB','userNameConverter', function($rootScope, $q, config, pouchDBFactory,userNameConverter){
        

        function getProgressListDocument(userDb){
            var def = $q.defer();
            userDb.get(config.idOfProgressList).then(function(result){
               def.resolve(result);
            }).catch(function(result){
                if(result.status == 404){
                    var progressList = config.genericProgressList;
                    progressList._id = config.idOfProgressList;
                    progressList.type = config.progresslisttype;
                    userDb.put(config.genericProgressList).then(function(result){
                        progressList._rev=result.rev;
                        def.resolve(result);
                    });
                }
                else{
                    def.reject();
                }
            });
            return def.promise;
        }

        function getNumberOfCardsConsideredDone(userdb,deckId,date ){
            return userdb.query(config.viewForDoneCheck, {
                startkey: [deckId, date],
                endkey: [deckId,{}],
                reduce: true
            });
        }

        function getNumberOfCardsInDeck(deckid){
            var deckDB = PouchDB(deckid);
            return deckDB.query(config.viewForCountingCards, {
                reduce: true
            });
        }

        function getCountFromResults(result){
            if(result.rows[0]){
                return result.rows[0].value || 0;
            }
            return 0;
        }

        function checkIfDeckIsDone(userdb,deckId){
            var considereDoneDate = new Date();
            var def = $q.defer();
            considereDoneDate.setDate(considereDoneDate.getDate() + config.considerDoneAfterDays);

            getNumberOfCardsConsideredDone(userdb,deckId, considereDoneDate).then(function(cardsConsideredDone){
                getNumberOfCardsInDeck(deckId).then(function(totalNumberOfCardsInDeck){
                    def.resolve(getCountFromResults(cardsConsideredDone) === getCountFromResults(totalNumberOfCardsInDeck));
                });
            });
            return def.promise;
        }
        var updatingHighScore = undefined;

        var updateService = {

            /**
             * Initializes all variables
             */


            init: function(){
                this.userDb = undefined;
                updatingHighScore = undefined;
            },

            /**
             * Return userdb
             */

            getUserDb: function(){
                this.userDb = this.userDb || pouchDBFactory(userNameConverter($rootScope.user));
                return this.userDb;
            },

            /**
             * Checks wehter Deck Is Considered Done
             */

            /**
             * Updates Progress List after learning took place
             * returns promise which resolves to true If Deck can be considered done now.
             */
            checkIfDeckIsConsideredDone: function(deckId){
              var def = $q.defer();
                var self = this;
                console.log(config);
                getProgressListDocument(self.getUserDb()).then(function(progressListDocument){
                   if(progressListDocument.skills[deckId] && progressListDocument.skills[deckId].done){
                       def.resolve(true);
                   } else{
                       progressListDocument.skills[deckId] = progressListDocument.skills[deckId] || {};
                       checkIfDeckIsDone(self.getUserDb(),deckId).then(function(isDone){
                           if(isDone){
                               progressListDocument.skills[deckId].done = true;
                               self.addPointsToHighScoreOfDeck(deckId,100,$rootScope.user);
                               def.resolve(true);
                           }
                           else{
                               progressListDocument.skills[deckId].done = false;
                               self.addPointsToHighScoreOfDeck(deckId,5,$rootScope.user);
                               def.resolve(false);
                           }
                           self.getUserDb().put(progressListDocument);
                       });
                   }
                });
                return def.promise;
            },
            addPointsToHighScoreOfDeck: function(deckid, score, user){
                var self = this;
                if(updatingHighScore){
                    updatingHighScore.then(function(result){
                        self.addPointsToHighScoreOfDeck(deckid,score,user);
                    });
                }
                updatingHighScore = $q.defer();
                var commonDb = pouchDBFactory("common");
                commonDb.get(user+"-"+deckid).then(function(result){
                    result.score = result.score+score;
                    commonDb.put(result).then(function(){
                        updatingHighScore.resolve();
                        updatingHighScore = undefined;
                    });
                }).catch(function(result){
                    var doc = {
                        _id:user+"-"+deckid,
                        user: user,
                        skill: deckid,
                        type: "highscore",
                        score: score
                    }
                    commonDb.post(doc).then(function(result){
                        updatingHighScore.resolve();
                        updatingHighScore = undefined;
                    })
                });
                return updatingHighScore.promise;
            }

        };
        return updateService;


    }]);

})();