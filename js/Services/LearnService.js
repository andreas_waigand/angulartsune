(function(){

    var app = angular.module("learn", ["pouch.cards", "pouch.update"]);

    app.service("learnService",["pouchDBService.cards", "pouchDBService.update", "$q", function(pouch, updateService, $q){
        var learnSessionComboDocuments = [];

        function updateComboDocumentAccordingToAnswerRating(comboDocument,rating){
            comboDocument.EF = SM2.methods.getEasienessFactor(comboDocument.EF, rating);
            comboDocument.repititions = comboDocument.repititions + 1;
            comboDocument.nextRepitition = SM2.methods.getNextRep(comboDocument.repititions, comboDocument.EF, rating);
            return comboDocument;
        }

        var learnService = {

            /**
             * @returns Current ComboDocument or undefined if it does not exist.
             */

            getCurrentCard: function(){
                return learnSessionComboDocuments[0];
            },

            rateQualityOfAnswer:function(quality){
                var lscd = learnSessionComboDocuments[0];
                if(quality === 0)
                    learnSessionComboDocuments.push(lscd);
                else
                    pouch.updateRepetitionInDb(updateComboDocumentAccordingToAnswerRating(lscd, quality)).then(function(){
                        updateService.checkIfDeckIsConsideredDone(lscd.deck);
                    });
            },

            prepareLearnSession:function(deckId){
                var learnDef = $q.defer();
                learnSessionComboDocuments = [];

                pouch.getDueComboDocuments(deckId).then(function(documents){
                    learnSessionComboDocuments = documents;
                    learnDef.resolve();
                });

                return learnDef.promise;
            },

            /**
             * Returns true if there is a card
             * @returns {boolean}
             */
            hasCurrentCard: function(){
                //!! turns "falsey" value like undefined, null etc. into boolean value
                return !!learnSessionComboDocuments[0];
            },

            nextCard: function(){
                learnSessionComboDocuments.shift();
            }
        };

        return learnService;

    }]);

})();