(function(){
    var app = angular.module("pouch.decks", ["pouch.base", "pouch.startup"]);


    /**
     * decklistDoc - The document in each userdb which specifies the decks the user may learn
     * generalDoc - The document in each Deck Database which specifies the following information:
     *              title: Printable Decktitle (e.g. Trivia 1)
     *              description: An description of the Deck
     *              progress: Percentile of cards which have been studied (0-100)
     *              deckId: the deckID (e.g. trivia-1). Also the name of the database.
     */

    app.constant("pouchDBService.decks.config", {
        decklistDoc: "openlist",
        generalDoc: "general",
        commonDB: "common"
    });

    app.factory("Deck", function DeckFactory(){
        return function(deckid, decktitle, deckdescription, cards){
            return {
                "id" : deckid,
                "title": decktitle,
                "description": deckdescription,
                "cards": cards
            }
        }
    });

    app.constant("pouchDBService.decks.cardview", {
        _id: '_design/cardDocs',
        views: {
            'frontback': {
                map: function(doc) {
                    if(doc._id != "general")
                        emit(doc._id, {"front":doc.front, "back":doc.back, "_rev":doc._rev});
                }.toString()
            },
            'countCards':{
                map: function(doc){
                    if(doc._id != "general")
                        emit(0,0);
                }.toString(),
                "reduce": "_count"
            }
        }
    })

    app.service("pouchDBService.decks", ['$q','pouch.config','pouch.base.config', '$rootScope', '$http', "pouchDBService.decks.config", "pouchDB", "Deck", "pouchDBService.decks.cardview","userNameConverter", function($q,mainconfig,baseconfig, $rootScope, $http, config, pouchDBFactory, deckFactory, cardView,userNameConverter){


        var DeckIDs = undefined;
        var DecksGeneralInformation = undefined;
        var updatingHighScore = undefined;

        var pouchDecks = {

            /**
             * Initializes all variables.
             * This will primarly be called when a user logs in our out.
             */

            init: function(){
                DeckIDs = undefined;
                DecksGeneralInformation = undefined;
                allDbs = undefined;
            },

            /**
             * Returns a promise for the DeckIDs as an array
             * DeckIDs are also the names of the deck-specific Databases
             * @return Promise for Deck Ids as Array
             */

            getDeckIDs: function(){
                return DeckIDs || getDeckIDsFromLocalDb();
            },

            getAllExistingDbs: function(){
                return getAllDbs();
            },
            /**
             *
             * @returns Promise for General Information on all Decks (viewable by the user) as Array
             */
            getDecksGeneralInformation: function(){
                return DecksGeneralInformation || getDecksGeneralInformationFromLocalDb();
            },

            getSingleDeckGeneralInformation: function(deckid){
              return getSingleDeckGeneralInformationFromRemoteDb(deckid)
            },

            /**
             * Creates a new deckdb and fills it with cards
             * @param deck
             */

            createDeck: function(deckid, decktitle,deckdescription,cards){
                var deck = deckFactory(deckid,decktitle,deckdescription,cards);
                var newdeck = pouchDBFactory(mainconfig.host + "/"+deck.id);
                var generalDb = pouchDBFactory(mainconfig.host + "/"+"general")
                var def = $q.defer();
                var ret = newdeck.put({
                    _id: "general",
                    title: deck.title,
                    description: deck.description,
                    "deckId": deck.id
                });
                ret.then(function(result){
                    newdeck.bulkDocs(deck.cards).then(function(){
                        PouchDB.replicate(newdeck, pouchDBFactory(deck.id)).then(function(){
                            def.resolve(result);
                        });
                    });
                }).then(function(){
                    newdeck.put(cardView);
                }).then(function(){
                    generalDb.put(
                        {
                            _id:deck.id,
                            title:deck.title,
                            description: deck.description,
                            responsible: baseconfig.module_name,
                            type:baseconfig.skilltype
                        }
                    )
                    ;
                });
                return def.promise;

            },



            updateDeck: function(deckid, decktitle,deckdescription,cards, _rev){
                var deck = deckFactory(deckid,decktitle,deckdescription,cards);
                var def = $q.defer();
                var updatedeck = pouchDBFactory(mainconfig.host + "/"+deck.id);
                var ret = updatedeck.put({
                    _id: "general",
                    title: deck.title,
                    description:deck.description,
                    deckId: deck.id,
                    _rev: _rev
                });
                ret.then(function(result){
                    if(cards.length > 0){
                        updatedeck.bulkDocs(cards).then(function(){
                            PouchDB.replicate(updatedeck, pouchDBFactory(deck.id)).then(function(){
                                def.resolve(result);
                            });

                        });
                    }else{
                        PouchDB.replicate(updatedeck, pouchDBFactory(deck.id));
                        def.resolve(result);
                    }
                });
                return def.promise;
            },

            /**
             * @params deckId
             * @returns Promise which resolves if deckId exists
             */

            checkIfDeckIdExists: function(deckId){
                var deckIdExistsDef = $q.defer();

                this.getDeckIDs().then(function(deckIDs){
                    if (deckIDs.indexOf(deckId) > -1) {
                        deckIdExistsDef.resolve();
                    }
                    else{
                        deckIdExistsDef.reject();
                    }
                });

                return deckIdExistsDef.promise;
            }

        }


        //Private Functions

        function getDeckIDsFromLocalDb(){
            var deckIDsDefere = $q.defer();
            var userDB = pouchDBFactory(userNameConverter($rootScope.user));
            userDB.get(config.decklistDoc).then(function(decklistDoc){
                if(decklistDoc[baseconfig.module_name])
                    deckIDsDefere.resolve(Object.keys(decklistDoc[baseconfig.module_name]));
                else{
                    deckIDsDefere.resolve([]);
                }
            });
            DeckIDs = deckIDsDefere.promise;
            return DeckIDs;
        }

        function getAllDbs(){
            return $http.get(mainconfig.host +"/_all_dbs");
        }

        function getSingleDeckGeneralInformationFromRemoteDb(deckID){
            var remoteDeckDb = pouchDBFactory(mainconfig.host + "/" + deckID);
            return remoteDeckDb.get(config.generalDoc);
        }

        function getSingleDeckGeneralInformationFromLocalDb(deckID){
            var deckDB = pouchDBFactory(deckID);
            return deckDB.get(config.generalDoc);
        }

        function getDecksGeneralInformationFromLocalDb(){
            var allDecksGeneralInformationCombinedDefer = $q.defer();
            var waitForAllDeckGeneralInformationPromisesDefer = $q.defer();
            var allDeckGeneralInformationPromises = [];
            pouchDecks.getDeckIDs().then(function(deckIDs){
                angular.forEach(deckIDs, function(deckID){
                    allDeckGeneralInformationPromises.push(getSingleDeckGeneralInformationFromLocalDb(deckID));
                });
                waitForAllDeckGeneralInformationPromisesDefer.resolve();
            });

            waitForAllDeckGeneralInformationPromisesDefer.promise.then(function(){
               $q.all(allDeckGeneralInformationPromises).then(function(allDecksGeneralInformation){
                   allDecksGeneralInformationCombinedDefer.resolve(allDecksGeneralInformation);
               });
            });

            DecksGeneralInformation = allDecksGeneralInformationCombinedDefer.promise;
            return DecksGeneralInformation;
        }

        return pouchDecks;


    }]);



})();