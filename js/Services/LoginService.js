(function () {

    var app = angular.module('login', ["pouch.login", "ipCookie"]);

    app.service('loginService', ['$rootScope', '$q', '$location', 'pouchDBService.login','$http', 'pouch.config', 'ipCookie' ,function ($rootScope, $q, $location, pouch,$http, config, cookie) {

        function loginToPouch(username, password) {
            return pouch.loginToRemoteDb(username, password);
        }

        function checkIfOfflineDbExists(username) {
            return pouch.checkIfOfflineDbExists(username);
        }

        function loginStateFactory(online, valid) {
            return {
                online: online,
                valid: valid
            };
        }




        var loginService = {
            username: "",

            startListeningToRouteChance: function () {
                var self = this;
                $rootScope.$on("$routeChangeStart", function (event, next, current) {
                    if ((!self.loggedIn && !$rootScope.user && !$rootScope.validationSuccess && !cookie("username")) && next.templateUrl != "snippets/login.html") {
                        self.plannedRoute = $location.path();
                        $location.path("/login");

                    }
                });
            },

            setLoggedIn: function(bool){
              this.loggedIn = true;
            },

            /**
             * Returns promise which resolves to "offline|online and valid|invalid"
             * @param username
             * @param password
             */
            login: function (username, password) {
                var loginDef = $q.defer();
                var self = this;
                var returnVal = loginToPouch(username, password);
                returnVal.then(function (res) {

                    $http.get(config.host + "/_session").then(function(result){
                        console.log(result);
                    });
                    self.loggedIn = true;
                    $rootScope.validationSuccess = true;
                    loginDef.resolve(loginStateFactory("online", "valid"));
                }).catch(function (error) {

                    //Invalid Login
                    if (error.status == "401") {
                        loginDef.resolve(loginStateFactory("online", "invalid"));
                        self.loggedIn = false;
                        $rootScope.validationSuccess = false;
                    }
                    //Offline...
                    else {
                        checkIfOfflineDbExists(username).then(function (result) {
                            //but offline db exists!
                            loginDef.resolve(loginStateFactory("offline", "valid"));
                            self.loggedIn = true;
                            $rootScope.validationSuccess = true;
                        }).catch(function (error) {
                            loginDef.resolve(loginStateFactory("offline", "invalid"));
                            self.loggedIn = false;
                            $rootScope.validationSuccess = false;
                        });

                    }
                });

                return loginDef.promise;
            },
            logout: function(username){
                pouch.logoutOfPouchDb(username);
                this.loggedIn = false;
                if(cookie("username"))
                    document.cookie = "username" + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
                $rootScope.validationSuccess = false;
                $rootScope.user = undefined;
            }


        };

        return loginService;


    }]);
})();
