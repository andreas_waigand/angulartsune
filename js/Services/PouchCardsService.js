(function(){
    var app = angular.module("pouch.cards", ["pouch.base"]);

    /**
     * Provides all functionality needed for a single learn session.
     * nextCard - Returns the next card document or "undefined" when done
     * updateRepititionInfo - Updates the repition info of card
     * prepareLearnSessionForDeck - Prepares Session for current deck
     */

    /**
     * Example Repitition Document in CouchDB
     * {
          "_id": "0452FF57-1E12-769E-B511-828AF9690875",
          "_rev": "2-44e3836e384dff3b115ca96158e49287",
          "cardid": "82192a2ea2ad6797fd2680278900216b",
          "EF": 2.2199999999999998,
          "rep": 3,
          "nextRep": "2015-03-13T09:24:26.202Z",
          "type": "repitition",
          "deck": "trivia-1"
        }
     */

    /**
     * Example Card Document in CouchDB
     *
     * {
          "_id": "DAFE0F71-C0D8-D992-8197-581E2E54CC0F",
          "_rev": "13-23a1abd35b490f37550c98eba9e03e56",
          "front": "Unter welchem Präsidenten der USA wurde der 22. Artikel der Verfassung ratifiziert, nachdem es einem Präsidenten nicht erlaubt ist mehr als zwei Amtszeiten zu haben?",
          "back": "Herry Truman"
     }
     */

    app.factory('repititionDocument', function repititionDocumentFactory() {
        return function (comboDocument) {
            return {
                "_id": comboDocument.repid,
                "_rev": comboDocument.reprev,
                "cardid": comboDocument.cardid,
                "EF": comboDocument.EF,
                "rep": comboDocument.repititions,
                "nextRep": comboDocument.nextRepitition,
                "type": "repitition",
                "deck": comboDocument.deck
            };
        };
    });

    app.factory('newRepititionDocument', function newRepititionDocumentFactory() {
        return function (cardid, deckid) {
            return {
                "cardid": cardid,
                "EF": 2.5,
                "rep": 1,
                "nextRep": new Date().toJSON(),
                "type": "repitition",
                "deck": deckid
            };
        };
    });



    app.factory('cardDocument', function cardDocumentFactory() {
        return function (cardid, front, back, _rev) {
            return {
                "_id": cardid,
                "front":front,
                "back": back,
                "_rev": _rev
            };
        };
    });

    app.factory('comboDocument', function comboDocumentFactory() {
        return function (carddocument, repdocument) {
            return {
                "cardid": carddocument._id,
                "cardrev": carddocument._rev,
                "front": carddocument.front,
                "back": carddocument.back,
                "repid": repdocument._id,
                "reprev": repdocument._rev,
                "EF": repdocument.EF,
                "repititions": repdocument.rep,
                "nextRepitition": repdocument.nextRep,
                "deck": repdocument.deck
            };
        };
    });

    app.constant("pouch.cards.config", {
        queryForDateAndDeck: "id/byDateAndDeck",
        queryForCardDocs: "cardDocs/frontback"
    });


    app.service("pouchDBService.cards", ['$rootScope', "$q", "pouch.cards.config","comboDocument",'cardDocument','repititionDocument','newRepititionDocument', 'pouchDB',"userNameConverter", function($rootScope, $q, config, comboDocumentFactory, cardDocumentFactory, repititionDocumentFactory, newRepDocFactory, pouchDBFactory, userNameConverter){

        var userDb = undefined;

        function getCurrentDate(){
            return new Date().toJSON();
        }

        function logError(error){
            console.log("error here", error);
        }

        function getAllRepititionDocuments(deckId){
            var allRepDef = $q.defer();
            userDb.query(config.queryForDateAndDeck, {
                startkey: [deckId],
                endkey: [deckId,{}]
            }).then(function(allReps){
                allRepDef.resolve(allReps.rows);
            }).catch(logError);
            return allRepDef.promise;
        }

        function getAllDueRepititionDocumentsForDeck(deckId){
            var allDueRepDef = $q.defer();
            userDb.query(config.queryForDateAndDeck, {
                startkey: [deckId],
                endkey: [deckId, getCurrentDate()]
            }).then(function(allDueReps){
                allDueRepDef.resolve(allDueReps.rows);
            }).catch(logError);
            return allDueRepDef.promise;
        }

        function makeHashmapFromCardDocuments(cardDocs){
            var hashMap = {};
            for( var i = 0; i < cardDocs.length; i++) {
                hashMap[cardDocs[i].id] = cardDocumentFactory(cardDocs[i].id, cardDocs[i].value.front,cardDocs[i].value.back,cardDocs[i].value._rev);
            }
            return hashMap;
        }

        function getAllCardDocumentsForDeck(deckId){
            var allCarddocsDef = $q.defer();
            var deckDb = pouchDBFactory(deckId);
            deckDb.query(config.queryForCardDocs).then(function(allCardDocs){
                var map = makeHashmapFromCardDocuments(allCardDocs.rows);
                allCarddocsDef.resolve(map);
            }).catch(logError);
            return allCarddocsDef.promise;
        }

        function getAllCardIdsFromRepititionDocuments(repititionDocuments){
            var allDueCardIds = [];
            for(var i = 0; i < repititionDocuments.length; i++){
                allDueCardIds.push(repititionDocuments[i].value.cardid);
            }
            return allDueCardIds;
        }


        var learnService = {

            /**
             * Initializes all variables
             */
            init: function(){
                userDb = undefined;
            },

            /**
             * Updates the given comboDocuments Repitition Document in DB
             * If it is a new document (and thus has no repid) it will create a new one.
             * @param comboDocument
             * @param quality
             */

            updateRepetitionInDb: function(comboDocument){
                if(comboDocument.reprev){
                    return userDb.put(repititionDocumentFactory(comboDocument));
                }
                else{
                    return userDb.post(repititionDocumentFactory(comboDocument));
                }
            },

            getAllCardDocuments: function(deckId){
              return getAllCardDocumentsForDeck(deckId);
            },



            /**
             * Returns a promise wihch resolves to all learnSessionComboDocuments.
             * @param deckId
             */

            getAllComboDocuments: function(deckId){
                var allCardsDef = $q.defer();
                userDb = userDb || pouchDBFactory(userNameConverter($rootScope.user));
                var allComboDocuments = [];

                getAllCardDocumentsForDeck(deckId).then(function(hashMapOfAllCardDocumentsByCardId){
                    getAllRepititionDocuments(deckId).then(function(allReps){
                        for(var i = 0; i < allReps.length; i++){
                            var repititionDocument = allReps[i].value;
                            allComboDocuments.push(comboDocumentFactory(hashMapOfAllCardDocumentsByCardId[repititionDocument.cardid],repititionDocument));
                            delete hashMapOfAllCardDocumentsByCardId[repititionDocument.cardid];
                        }
                        for(var cardWithoutRepDocId in hashMapOfAllCardDocumentsByCardId){
                            allComboDocuments.unshift(comboDocumentFactory(hashMapOfAllCardDocumentsByCardId[cardWithoutRepDocId], newRepDocFactory(cardWithoutRepDocId,deckId)));
                        }
                        allCardsDef.resolve(allComboDocuments);
                    });
                });
                return allCardsDef.promise;
            },

            /**
             * Returns a promise which resolves to all learnSessionComboDocuments which are due.
             * @param deckId
             */
            getDueComboDocuments: function(deckId){
                var learnSessionDef = $q.defer();


                this.getAllComboDocuments(deckId).then(function(allComboDocs){
                    var currentDate = new Date();
                    var learnDocs = [];
                    for(var i = 0; i < allComboDocs.length; i++){
                        if(currentDate >= new Date(allComboDocs[i].nextRepitition)){
                            learnDocs.push(allComboDocs[i]);
                        }
                        else{
                            break;
                        }
                    }
                    learnSessionDef.resolve(learnDocs);
                });

                return learnSessionDef.promise;
            }


        };
        return learnService;


    }]);

})();