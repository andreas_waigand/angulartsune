(function () {
    var app = angular.module("pouch.login", ["pouch.startup", "pouch.base"]);


    /**
     * This Service's only purpose is to login to a remote Couchdb and
     * get an AuthToken to identify oneself.
     * This Service also creates the design/docs for the user if they have not yet been created.
     */


    app.constant("pouch.templates", [{

        "_id": "_design/tsune",
        "views": {
            "countRepsInPeriod": {
                "map": "function(doc) {\n  if(doc.type === \"repitition\")\n  emit([doc.deck,doc.nextRep], 1);\n}",
                "reduce": "_count"
            }

        }
    },{
        "_id": "_design/id",
        "language": "javascript",
        "views": {
            "allIds": {
                "map": "function(doc) {\n  if(doc.type==\"repitition\") {\n  emit(doc.cardid, 1);\n  }\n}"
            },
            "byDate": {
                "map": "function(doc) {\n  if(doc.type==\"repitition\"){\n  emit(doc.nextRep, doc);\n  }\n}"
            },
            "byDateAndDeck": {
                "map": "function(doc) {\n  if(doc.type==\"repitition\")\n    emit([doc.deck,doc.nextRep],doc);\n}"
            }
        }
    }

    ])


    app.service("pouchDBService.login", ["pouch.config", "pouchDBService", '$q', 'pouchDB',"pouch.templates", "userNameConverter", function (config, pouch, $q, pouchDBFactory,templates,userNameConverter) {


        function checkIfDesignDocsExist(username){
            var def = $q.defer();
            var remoteDb = pouchDBFactory(config.host +  userNameConverter(username));
            remoteDb.get("_design/id").then(function(result){
                def.resolve(true);
            }).catch(function(error){
                def.resolve(false);
            });

            return def.promise;
        }

        function createDesignDocs(username){
            var def = $q.defer();
            var remoteDb = pouchDBFactory(config.host + userNameConverter(username));
            remoteDb.bulkDocs(templates).then(function(result){
                def.resolve(true);
            }).catch(function(error){
                def.resolve(false);
            });
            return def.promise;

        }

        var pouchLogin = {

            loginDef: undefined,

            checkIfLoggedIn: function () {
                if (this.loginDef)
                    return this.loginDef.promise;
                var self = this;
                var remoteDb = pouchDBFactory(config.host + "/_session");
                self.loginDef = $q.defer();
                remoteDb.getSession().then(function (result) {
                    checkIfDesignDocsExist(result.userCtx.name).then(function(designDocsExist){
                        if(designDocsExist){
                            self.loginDef.resolve({online: true, name: result.userCtx.name});
                        }
                        else{
                            createDesignDocs(result.userCtx.name).then(function(){
                                self.loginDef.resolve({online: true, name: result.userCtx.name});
                            })
                        }
                    });

                }).catch(function (result) {
                    self.loginDef.resolve({online: false, name: undefined});
                });
                return this.loginDef.promise;
            },

            /**
             * @param username
             * @param password
             * @return Promise for login which can reflect three different outcomes.
             * 1. Status: 401 == WRONG Password
             * 2. Status: 200 == Everything is fine
             * 3. Status: Everything Else == Offline, Meteor-crash etc.
             */

            loginToRemoteDb: function (username, password) {
                var remoteDb = pouchDBFactory(config.host + userNameConverter(username));
                var loginDef = $q.defer();
                remoteDb.login(username, password).then(function (res) {
                    checkIfDesignDocsExist(username).then(function(designDocsExist){
                        if(designDocsExist){
                            loginDef.resolve(res);
                        }
                        else{
                            createDesignDocs(username).then(function(){
                                loginDef.resolve(res);
                            })
                        }
                    });

                }).catch(function (err) {
                    loginDef.reject(err);
                });
                return loginDef.promise;
            },

            /**
             * This function checks if an DB is available in local storage. It is primarly used to check
             * wether or not a Users DB is available in local storage.
             * It is currently checked by performing a query for "decklist", which should exist in every
             * UserDB.
             * TODO: Check against something more general.
             * @param nameOfPossibleOfflineDb
             * @returns {d.promise|promise|fd.g.promise|.n.promise}
             */

            checkIfOfflineDbExists: function (nameOfPossibleOfflineDb) {
                var checkDef = $q.defer();
                var possibleOfflineDb = pouchDBFactory(userNameConverter(nameOfPossibleOfflineDb));
                possibleOfflineDb.get("decklist").then(function (res) {
                    checkDef.resolve();

                }).catch(function (error) {

                    checkDef.reject();
                    PouchDB.destroy(userNameConverter(nameOfPossibleOfflineDb));
                });

                return checkDef.promise;

            },


            logoutOfPouchDb: function (username) {
                var self = this;
                var remoteDb = pouchDBFactory(config.host + userNameConverter(username));
                pouch.stopReplication();
                pouch.resetAllField();
                remoteDb.logout(function (err, response) {
                    if (err) {
                        // TODO: network error
                    }
                });
            }


        }
        return pouchLogin;


    }]);


})();