var SM2 = SM2 || {};

SM2.methods = {
    getInterrepetitionIntervall: function(repitition, EF){
        if (repitition <= 2){
            return repitition;
        }
        else{
            return this.getInterrepetitionIntervall(repitition-1, EF)*EF;
        }
    },
    getNextRep : function(numberOfRepeats,EF, quality){
        var factor = 1;
        if(quality < 3){
            factor= SM2.methods.getInterrepetitionIntervall(1,EF);
        }
        else{
            factor = SM2.methods.getInterrepetitionIntervall(numberOfRepeats,EF);
        }
        return new Date(new Date().getTime() + factor * 86400000).toJSON();
    },
    /**
     * This methods computes the new easiness factor of a smallest possible information unit (card)
     * based on the self percieved quality of the answer given by the learner.
     * @param {float} oldEF The old easiness factor
     * @param {integer} Quality The quality of the students answer. Range from 0-5, where 0 is worst.
     * @returns {float} The new Easyfaktor
     */
    getEasienessFactor: function(oldEF,Quality){
        var result = oldEF+(0.1-(5-Quality)*(0.08+(5-Quality)*0.02));

        if(result < 1.1)
            result = 1.1;
        else if(result > 2.5)
            result = 2.5;
        return result;
    }
};
