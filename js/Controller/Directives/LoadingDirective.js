(function(){
var app = angular.module("busyCog",[]);

app.directive("busyCog", ['$timeout', function ($timeout) {
    return {
        restrict: 'E',
        template: '<i class="fa fa-cog fa-spin ng-hide" ng-show="show"></i>',
        scope: {
            show: '='
        }
    };
}]);
})();