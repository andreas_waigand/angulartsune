
(function(){

    var app = angular.module('cardCtrl', ['pouch.cards', 'learn']);

    app.controller("CardController", ['$routeParams','$scope', 'pouchDBService.cards', function ($routeParams,$scope, pouch) {

        $scope.timeDiff = function(date){
            var milliseconds = new Date(date) - new Date();
            return milliseconds/1000;
        }

        pouch.getAllComboDocuments($routeParams.deckTitle).then(function (res) {
            $scope.cards = res;
        });


    }]);

})();


