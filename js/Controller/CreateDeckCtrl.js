(function(){

    var app = angular.module("createDeckCtrl", ['pouch.decks', 'pouch.cards']);

    app.controller("CreateDeckController", ["$scope", "$routeParams", "pouchDBService.decks", "pouchDBService.cards",function($scope,$routeParams, pouch, pouchCards){


        function initialize(){
            $scope.id = "";
            $scope.title = "";
            $scope.description = "";
            $scope.cards = [];
        }

        pouch.getAllExistingDbs().then(function(allExistingDbs){
            if($routeParams.deckId){
                if(allExistingDbs.data.indexOf($routeParams.deckId) > -1)
                {
                    $scope.lockId = true;
                    pouch.getSingleDeckGeneralInformation($routeParams.deckId).then(function(singleDeck){
                        pouchCards.getAllCardDocuments($routeParams.deckId).then(function(allCards){
                            $scope.deckids = allExistingDbs.data;
                            $scope.id = singleDeck.deckId;
                            $scope._rev = singleDeck._rev;
                            $scope.title = singleDeck.title;
                            $scope.cards = [];
                            $scope.description = singleDeck.description;
                            for(cards in allCards){
                                $scope.cards.push(allCards[cards]);
                            }
                        });

                    });
                }else{
                    $scope.deckids = allExistingDbs.data;
                    initialize();
                    $scope.id = $routeParams.deckId;
                }
            }
            else{
            $scope.deckids =  allExistingDbs.data;
                initialize();
            }
        });




        $scope.create = function(){
            $scope.updating = true;
            if($scope._rev){
                var ret = pouch.updateDeck($scope.id, $scope.title, $scope.description, $scope.cards, $scope._rev);
                ret.then(function(result){
                   $scope._rev = result.rev;
                    pouchCards.getAllCardDocuments($scope.id).then(function(allCards){
                        $scope.cards = [];
                        for(cards in allCards){
                            $scope.cards.push(allCards[cards]);
                        }
                        $scope.updating = false;
                        $scope.success = true;
                    });
                }).error(function(){
                    $scope.error=true;
                });;
            }else{
                var ret = pouch.createDeck($scope.id,$scope.title,$scope.description, $scope.cards);
                ret.then(function(result){
                    $scope._rev = result.rev;
                    $scope.lockId = true;
                    pouchCards.getAllCardDocuments($scope.id).then(function(allCards){
                        $scope.cards = [];
                        for(cards in allCards){
                            $scope.cards.push(allCards[cards]);
                        }
                        $scope.updating = false;
                        $scope.success = true;
                    });
                }).error(function(){
                    $scope.error=true;
                });
            }
        }

        $scope.add = function(){
            $scope.cards.push({"front":"", "back":""});
        }

    }]);

})();
