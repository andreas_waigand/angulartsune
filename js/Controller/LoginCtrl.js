(function(){

    var app = angular.module("loginCtrl", ['login', 'pouch.startup', 'busyCog', 'ipCookie']);

    app.controller("LoginController", ['$scope', 'loginService','pouchDBService', '$rootScope', '$location', 'ipCookie', function ($scope, loginService, pouch, $rootScope, $location, ipCookie) {
        $scope.user = {};
        $scope.isBusy=false;
        $scope.displayLoginError = function () {
            $scope.loginError = true;
        };



        $scope.login = function () {
            $scope.isBusy = true;
            loginService.login($scope.user.username, $scope.user.password).then(function (result) {
                if (result.valid == "valid") {
                    if($scope.user.remember){
                        document.cookie="username={\"name\":\""+$scope.user.username+"\"};expires=Fri, 3 Aug 2200 20:47:11 UTC;"
                    }else{
                        document.cookie = "username" + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
                    }
                    $rootScope.user = $scope.user.username;
                    var startupProm = pouch.generalStartup(result.online == "online", $scope.user.username);
                    startupProm.then(function(res){
                        loginService.loggedIn = true;
                        if(loginService.plannedRoute)
                            $location.path(loginService.plannedRoute);
                        else
                            $location.path("/decklist");

                    });
                } else {
                    console.log(result.online + " " + result.valid);
                    $scope.isBusy=false;
                    $scope.displayLoginError();
                }

            });

        };


    }]);

})();


