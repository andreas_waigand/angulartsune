(function(){

    var app = angular.module("learnCtrl", ['learn', 'pouch.decks']);

    app.constant("learnCtrl.messages", {
        endOfDeck: "You're done!",
        deckDoesNotExist: "This deck does not exist!"
    });

    app.filter('readableTime', function() {
        function roundToTwoDigets(number){
            return Math.round(number * 100) / 100;
        }

        return function(seconds) {
            var day, format, hour, minute, month, week, year;
            seconds = parseInt(seconds, 10);
            minute = 60;
            hour = minute * 60;
            day = hour * 24;
            week = day * 7;
            year = day * 365;
            month = year / 12;
            format = function(number, string) {
                string = number === 1 ? string : "" + string + "s";
                return "" + number + " " + string;
            };
            switch (false) {
                case !(seconds < minute):
                    return format(seconds, 'second');
                case !(seconds < hour):
                    return format(roundToTwoDigets(seconds / minute), 'minute');
                case !(seconds < day):
                    return format(roundToTwoDigets(seconds / hour), 'hour');
                case !(seconds < week):
                    return format(roundToTwoDigets(seconds / day), 'day');
                case !(seconds < month):
                    return format(roundToTwoDigets(seconds / week), 'week');
                case !(seconds < year):
                    return format(roundToTwoDigets(seconds/month), 'month');
                default:
                    return format(roundToTwoDigets(seconds / year), 'year');
            }
        };
    });

    app.controller("LearnController", ['$routeParams', '$scope', 'learnService', "pouchDBService.decks", "learnCtrl.messages", function ($routeParams, $scope, learnService, decksService, msg) {


        function repTimeForButtons(EF, reps){
            var buttons=[];
            buttons[1] = SM2.methods.getInterrepetitionIntervall(1,SM2.methods.getEasienessFactor(EF,1)) * 86400;
            for (var i = 2; i < 6; i++){
                var newEF= SM2.methods.getEasienessFactor(EF,i);
                buttons[i] = SM2.methods.getInterrepetitionIntervall(reps,newEF) * 86400;
            }
            return buttons;
        }

        function updateButtons(){
            $scope.buttons = repTimeForButtons(learnService.getCurrentCard().EF, learnService.getCurrentCard().repititions);
        }

        $scope.showingButtonBar = false;
        $scope.deckDone =false;

        decksService.checkIfDeckIdExists($routeParams.deckTitle).then(function(){
            learnService.prepareLearnSession($routeParams.deckTitle).then(function () {
                $scope.showQuestion();
                $scope.isReady = true;
            });
        }).catch(function(){
            $scope.cardtext = msg.deckDoesNotExist;
            $scope.isReady = true;
        });

        $scope.showingAnswer = false;

        $scope.rateAnswer = function (quality) {
            learnService.rateQualityOfAnswer(quality);
            learnService.nextCard();
            $scope.showingAnswer = false;
            $scope.showQuestion();
        };

        $scope.showQuestion = function(){
            if(learnService.hasCurrentCard()){
                $scope.cardtext= learnService.getCurrentCard().front;
                updateButtons();
                $scope.showingButtonBar = true;
            }else{
                $scope.deckDone = true;
                $scope.cardtext = msg.endOfDeck;
                $scope.showingButtonBar = false;
            }
        };

        $scope.showAnswer = function () {
            $scope.cardtext = learnService.getCurrentCard().back;
            $scope.showingAnswer = true;
        };

    }]);

})();

