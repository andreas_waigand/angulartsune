
(function(){

    var app = angular.module('deckCtrl', ['pouch.decks']);

    app.controller("DeckController", ['$scope', 'pouchDBService.decks', '$location', function ($scope, pouch, $location) {

        pouch.getDecksGeneralInformation().then(function (res) {
            $scope.decks = res;
        });


        $scope.changeView = function (deckid) {
            var path = 'learn/' + deckid;

            $location.path(path);

        };

        $scope.changeToCardView = function(deckid){
            var path = 'cards/' + deckid;
            $location.path(path);
        };

        $scope.setProgress = function (progress) {
            return {
                width: progress + '%'
            };
        };

        $scope.setDanger = function (progress) {
            return {
                width: (100 - progress) + '%'
            };
        };

    }]);

})();


