(function(){
   var app = angular.module("navCtrl", ['login']);

    app.controller("NavController",['loginService', '$rootScope','$scope', function(login, $rootScope, $scope){

        $scope.loggedIn = login;

        $scope.logout = function(){
                login.logout($rootScope.user);
        };


    }]);
    app.directive("tsuNavigation", function(){
       return {
           templateUrl: "snippets/navUI.html",
           controller: "NavController"
       };
    });

})();