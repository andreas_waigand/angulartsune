
(function () {

    var app = angular.module('welcome', ['ngRoute', 'deckCtrl','cardCtrl','loginCtrl','learnCtrl','navCtrl',"createDeckCtrl",'login', 'ipCookie', 'pouch.base', 'pouch.login']);


    app.run(['loginService','ipCookie', "$rootScope", function (login, cookie, $rootScope) {
        if(cookie("username")){
            $rootScope.user = cookie("username").name;
            login.setLoggedIn(true);
        }
        else{
        login.loggedIn = false;
        login.startListeningToRouteChance();
        }
    }]);


    app.config(['$routeProvider',
        function ($routeProvider, login, $q) {
            $routeProvider.
                when('/decklist', {
                    templateUrl: 'snippets/deckUI.html',
                    controller: 'DeckController'
                    , controllerAs: 'deckCtrl',
                    resolve: {
                     pouch: ["pouchDBService",'$rootScope','pouchDBService.login','$q', function (pouchDBService,$rootScope,login,$q) {
                         var def = $q.defer();
                         login.checkIfLoggedIn().then(function(result){
                            pouchDBService.generalStartup(result.online, result.name || $rootScope.user).then(function(){
                                def.resolve();
                            });
                         });
                         return def.promise;
                     }]
                     }
                }).
                when("/cards/:deckTitle", {
                    templateUrl: 'snippets/cardsUI.html',
                    controller: 'CardController',
                    controllerAs: 'cardCtrl'
                }).
                when('/learn/:deckTitle', {
                    templateUrl: 'snippets/learnUI.html',
                    controller: 'LearnController',
                    controllerAs: 'learnCtrl',
                    resolve: {
                        pouch: ["pouchDBService",'$rootScope','pouchDBService.login','$q', function (pouchDBService,$rootScope,login,$q) {
                            var def = $q.defer();
                            login.checkIfLoggedIn().then(function(result){
                                pouchDBService.generalStartup(result.online, result.name || $rootScope.user).then(function(){
                                    def.resolve();
                                });
                            });
                            return def.promise;
                        }]
                    }
                    /*resolve: {
                     pouch: function (pouchDBService) {
                     return pouchDBService.startup();
                     }
                     }*/

                }).when('/login', {
                    templateUrl: 'snippets/login.html',
                    controller: 'LoginController',
                    controllerAs: 'loginCtrl'
                }).when('/editor/:deckId?', {
                    templateUrl: 'snippets/createDeckUI.html',
                    controller: 'CreateDeckController'
                })
                .otherwise({
                    redirectTo: '/decklist'
                });
        }]);






})();